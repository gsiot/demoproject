import os
import sys

FILE_NAME = 'all_files.rst'
BRANCH_FILE_NAME = 'all_branches.rst'
PATH = '../source'

f = []

for (dirpath, dirnames, filenames) in os.walk(PATH):
    f.extend(filenames)
    break

header_files = [file for file in f if file.endswith(('h','hpp'))]

print('Found flies: ', header_files)

def get_all_branches():
    pass

def get_rst_lvl1_header(header_name):
    rst_output = ""
    rst_output += header_name + "\n"
    rst_output += "^" * len(header_name) + "\n"
    rst_output += "\n"

    return rst_output

def get_rst_lvl2_header(header_name):
    rst_output = ""
    rst_output += header_name + "\n"
    rst_output += "+" * len(header_name) + "\n"
    rst_output += "\n"

    return rst_output

def get_doxygenfile_directive(header_name, project=None):
    rst_output = ".. doxygenfile:: "
    rst_output += header_name + "\n"
    if project: rst_output += "	:project: " + project + "\n"
    rst_output += "\n"

    return rst_output

def generate_file():
    with open(FILE_NAME, "w", encoding='utf-8') as file:
        file.write(get_rst_lvl1_header("All files index"))
        file.write('some text\n\n')

        for header_file_name in header_files:
            file.write(get_rst_lvl2_header(header_file_name))
            file.write(get_doxygenfile_directive(header_file_name, 'testproject'))

# def generate_branch_file():
#     with open(BRANCH_FILE_NAME, "w", encoding='utf-8') as file:
#         file.write(get_rst_lvl1_header("Other branches"))
#         file.write('some text about branches\n\n')
#
#         for branch_name, branch_link in get_all_branches():
#             file.write(get_rst_lvl2_header(branch_name))
#             file.write(get_doxygenfile_directive(branch_link, 'testproject'))


print('Generating file ', FILE_NAME)
generate_file()
# print('Generating file ', BRANCH_FILE_NAME)
# generate_branch_file()