/*
 * Copyright (c) 2017, NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file    MKW41Z512xxx4_OT.cpp
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKW41Z4.h"
#include "fsl_adc16.h"
#include "fsl_debug_console.h"
#include <FreeRTOS.h>
#include <task.h>
#include <assert.h>
#include <openthread-core-config.h>
#include <openthread/config.h>
#include <openthread/cli.h>
#include <openthread/diag.h>
#include <openthread/tasklet.h>
#include <openthread/thread.h>
#include <openthread/platform/logging.h>
#include "platform.h"
#include <app_timer.h>
#include <nrf_error.h>
#include <nrf_log.h>
#include <mqttsn_client.h>
#if ENABLE_RTT_CONSOLE
#include "SEGGER_RTT/SEGGER_RTT.h"
#endif

#include <openthread/diag.h>
#include <openthread/icmp6.h>
#include <openthread/platform/uart.h>

#include <openthread/include/openthread/icmp6.h>
#include <openthread/include/openthread/ip6.h>
#include <openthread/include/openthread/coap.h>
#include <openthread/include/openthread/error.h>
#include <openthread/src/core/common/code_utils.hpp>
#include <openthread/src/core/common/logging.hpp>
#include <openthread/src/core/common/encoding.hpp>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "fsl_adc16.h"
#include <openthread/include/openthread/platform/uart.h>
#include <openthread/src/core/common/logging.hpp>
#include <openthread/sntp.h>
#include <openthread/crypto.h>

#include "MKW41Z512xxx4_OT.hpp"
//
// Defines
//

#define main_task_PRIORITY (tskIDLE_PRIORITY + 1)
#define BOARD_LED_GPIO BOARD_LED_RED_GPIO
#define BOARD_LED_GPIO_PIN BOARD_LED_RED_GPIO_PIN
#define DEMO_ADC16_BASE ADC0
#define DEMO_ADC16_CHANNEL_GROUP 0U
#define DEMO_ADC16_USER_CHANNEL 4U /* PTB18, ADC0_SE4 */
#if ENABLE_RTT_CONSOLE
#define DOWN_BUFFER_SIZE 256
#endif

/**
 * SNTP server address.
 */
#define SNTP_QUERY_INTERVAL 120000
#define SNTP_SERVER_ADDRESS "64:ff9b::d8ef:2308"
#define SNTP_SERVER_PORT 123

/**
 * Google Cloud Platform CoAP server parameters.
 */
#define GCP_COAP_IOT_CORE_SERVER_ADDRESS "64:ff9b::2242:AE4B"
#define GCP_COAP_IOT_CORE_SERVER_PORT 5683
#define GCP_COAP_IOT_CORE_SERVER_SECURE_PORT 5684

/**
 * Google Cloud Platform project configuration.
 */
#define GCP_COAP_IOT_CORE_PATH "gcp"
#define GCP_COAP_IOT_CORE_PROJECT_ID "iot-school-2"
#define GCP_COAP_IOT_CORE_REGISTRY_ID "coap-demo"
#define GCP_COAP_IOT_CORE_REGION "us-central1"
#define GCP_COAP_IOT_CORE_PUBLISH "publishEvent"
#define GCP_COAP_IOT_CORE_SETSTATE "setState"
#define GCP_COAP_IOT_CORE_CONFIG "config"

/**
 * CoAP transport configuration
 */
#define GCP_COAP_SECURE_ENABLED 1
#define GCP_COAP_SECURE_PSK "some_secret"
#define GCP_COAP_SECURE_PSK_INDENTITY "my_identity"

/**
 * Device configuration config. Only for demonstration purposes.
 */
#define CONFIG_LED1 "{  \"binaryData\": \"TEVEMQ==\","
#define CONFIG_LED2 "{  \"binaryData\": \"TEVEMg==\","
#define CONFIG_LED3 "{  \"binaryData\": \"TEVEMw==\","
#define CONFIG_LED4 "{  \"binaryData\": \"TEVENA==\","

/**
 * Google Cloud Platform device configuration.
 */
#define GCP_COAP_IOT_CORE_DEVICE_ID "demo-device"
#define GCP_COAP_IOT_CORE_DEVICE_KEY                                                                                                                           \
	"-----BEGIN EC PRIVATE KEY-----\r\n"                                                                                                                       \
	"MHcCAQEEIG3g+2QEL3B76v463RAlToqC+2HWDDBZ9VHZ0d7j2qQboAoGCCqGSM49\r\n"                                                                                     \
	"AwEHoUQDQgAEJvVcqAnTdt/rUc8P10Zt+ceW/QRbzXRuMnQrnSXFfVtG3jOAR/Pk\r\n"                                                                                     \
	"ILpuR4/fGGQaBGZ60SoVlMorykYFRCtlGw==\r\n"                                                                                                                 \
	"-----END EC PRIVATE KEY-----\r\n"

/**
 * The JSON representation of the header with ES256 algorithm.
 */
#define JWT_HEADER_TYPE_ES256 "{\"alg\":\"ES256\",\"typ\":\"JWT\"}"

/**
 * The maximum size of the JWT signature.
 */
#define JWT_SIGNATURE_SIZE 64

/**
 * The size of key length for ES256.
 */
#define JWT_KEY_LENGTH_ES256 32

/**
 * The JWT delimeter used to separete header, claim and signature.
 *
 */
#define JWT_DELIMETER '.'

/**
 * Time obtained from SNTP.
 */
static uint64_t m_unix_time;

/**
 * Variable indicates if config has been requested.
 */
static bool m_config_requested = false;

//
// Function prototypes
//

void coap_handler_test(void* aContext, otMessage* aMessage, const otMessageInfo* aMessageInfo);
void coap_handler_lux_raw(void* aContext, otMessage* aMessage, const otMessageInfo* aMessageInfo);
void coap_handler_lux_dz(void* aContext, otMessage* aMessage, const otMessageInfo* aMessageInfo);
void coap_handler_lux_trig(void* aContext, otMessage* aMessage, const otMessageInfo* aMessageInfo);
void coapAppInit();
void getLightLevel();
void index_lookup(int index);
void PrintPayload(otMessage* aMessage);
void parsePostMessage(otMessage* aMessage);
void icmpcallback_handler(void* aContext, otMessage* aMessage, const otMessageInfo* aMessageInfo, const otIcmp6Header* aIcmpHeader);
void control_neigbor(uint32_t neighbor_index, lightTrigger_t light_trigger);

static void coap_publish(void);
static void coap_set_state(void);
static void coap_config_get(void);
static void sntp_query(void);
static void coap_default_handler(void* p_context, otMessage* p_message, const otMessageInfo* p_message_info);

//
//  Variables

static appMode_t app_mode = appMode_Init;
static otInstance* sInstance;
adc16_config_t adc16ConfigStruct;
adc16_channel_config_t adc16ChannelConfigStruct;
otCoapResource cr_1;        // coap resource 1
otCoapResource cr_led;      // coap resource for led
otCoapResource cr_mode;     // coap resource for mode
otCoapResource cr_lux_raw;  // coap resource to receive light level
otCoapResource cr_lux_dz;   // coap resource light level deadzone
otCoapResource cr_lux_trig; // coap resource light trigger level
otCoapResource cr_index;    // coap resource for index
otError error = OT_ERROR_NONE;
uint32_t light_level_adc; // current light level in adc readings
uint32_t light_index = 1; // index of a device
otIp6Address allCoapAddr;
const char* allCoapAddrStr = "FF0A::FD";
uint32_t light_lvl_trigger = 500;
uint32_t light_lvl_gyst = 200;
uint32_t light_lvl_last[10];
uint32_t count = 0;
uint32_t led_value = 0;
uint32_t delay_val = 0;
lightTrigger_t light_trigger = light_no_change;
TickType_t ticks, prev_ticks, slow_polling_ticks, led_on_timer, last_ticks, config_get_ticks, state_set_ticks;
otIcmp6Handler icmpHandler;

void led_control()
{
	TickType_t current_ticks, period;
	current_ticks = xTaskGetTickCount();
	period = (current_ticks - last_ticks);
	last_ticks = xTaskGetTickCount();

	if (led_on_timer > 0) // every 3000 ms
	{
		led_on_timer = led_on_timer - period;
		GPIO_SetPinsOutput(BOARD_LED_GPIO, 1u << BOARD_LED_GPIO_PIN);
	}
	else
	{
		// if state is not on, then off
		GPIO_ClearPinsOutput(BOARD_LED_GPIO, 1u << BOARD_LED_GPIO_PIN);
	}
}

void lightControl()
{
	getLightLevel();
	switch (app_mode)
	{
		case appMode_Init:
		{
			ticks = xTaskGetTickCount();
			if (ticks - prev_ticks > pdMS_TO_TICKS(2000)) // every 1000 ms
			{
				prev_ticks = xTaskGetTickCount();
			}
			break;
		}
		case appMode_Manual:
		{
			ticks = xTaskGetTickCount();
			break;
		}
		case appMode_Single:
		{
			ticks = xTaskGetTickCount();
			if (ticks - prev_ticks > pdMS_TO_TICKS(3000)) // every 3000 ms
			{
				prev_ticks = xTaskGetTickCount();
				switch (light_trigger)
				{
					case light_goes_up:
					{
						GPIO_ClearPinsOutput(BOARD_LED_GPIO, 1u << BOARD_LED_GPIO_PIN);
						break;
					}
					case light_goes_down:
					{
						GPIO_SetPinsOutput(BOARD_LED_GPIO, 1u << BOARD_LED_GPIO_PIN);
						break;
					}
					default: break;
				}
			}
			break;
		}
		case appMode_Multiple:
		{
			ticks = xTaskGetTickCount();
			//		fast polling
			if (ticks - prev_ticks > pdMS_TO_TICKS(5000))
			{
				prev_ticks = xTaskGetTickCount();
				otLogInfoPlat("Multiple mode, my index is %d", light_index);
				control_neigbor(light_index + 1, light_trigger);
				index_lookup(1);
			}
			break;
		}
		default: break;
	}
	if (ticks - config_get_ticks > pdMS_TO_TICKS(10000 + delay_val))
	{
		config_get_ticks = xTaskGetTickCount();
		sntp_query();
		coap_config_get();
	}
}

/*
 * @brief   Application entry point.
 */
int main(void)
{
	xTaskCreate(main_task, "Main", configMINIMAL_STACK_SIZE + 512 + 1024, NULL, main_task_PRIORITY, NULL);
	vTaskStartScheduler();
	for (;;)
		;
	return 0;
}

/*!
 * @brief Main task
 */
static void main_task(void* pvParameters)
{
	//	int* t = (int*)0xffffffff;
	//	*t = 55;
	int argc = 0;
	char* argv[] = { (char*)"", NULL };
#if ENABLE_RTT_CONSOLE
	char input[DOWN_BUFFER_SIZE];
	size_t index = 0;
	memset(input, 0x00, DOWN_BUFFER_SIZE);
#endif

	/* Init board hardware. */
	BOARD_InitBootPins();
	BOARD_InitBootClocks();
	BOARD_InitBootPeripherals();
	BOARD_InitDebugConsole();
	gpio_pin_config_t led_config = {
		kGPIO_DigitalOutput,
		0,
	};
	GPIO_PinInit(BOARD_LED_GPIO, BOARD_LED_GPIO_PIN, &led_config);
	ADC16_GetDefaultConfig(&adc16ConfigStruct);
	ADC16_Init(DEMO_ADC16_BASE, &adc16ConfigStruct);
	ADC16_EnableHardwareTrigger(DEMO_ADC16_BASE, false); /* Make sure the software trigger is used. */
	adc16ChannelConfigStruct.channelNumber = DEMO_ADC16_USER_CHANNEL;
	adc16ChannelConfigStruct.enableInterruptOnConversionCompleted = false;

pseudo_reset:

	// More init
	PlatformInit(argc, argv);
	//    otLogInfoPlat("Init instance\r\n");
	sInstance = otInstanceInitSingle();
	assert(sInstance);
	otCliUartInit(sInstance);
	otIp6SetEnabled(sInstance, true);

	// init COAP resources
	coapAppInit();

	// Subscribe to Multicast address
	otIp6AddressFromString(allCoapAddrStr, &allCoapAddr);
	otIp6SubscribeMulticastAddress(sInstance, &allCoapAddr);

	// register ICMPv6 handler
	icmpHandler.mReceiveCallback = icmpcallback_handler;
	otIcmp6RegisterHandler(sInstance, &icmpHandler);

	// установка таймера 10 s
	led_on_timer = pdMS_TO_TICKS(10000);
	prev_ticks = xTaskGetTickCount();
	slow_polling_ticks = xTaskGetTickCount();

	// Cycle over process
	while (!PlatformPseudoResetWasRequested())
	{
		otTaskletsProcess(sInstance);
		PlatformProcessDrivers(sInstance);

		lightControl(); // client app process
		                //        led_control();

		// Segger RTT process
#if (ENABLE_RTT_CONSOLE)
		if (SEGGER_RTT_HasKey())
		{
			input[index] = (char)SEGGER_RTT_GetKey();
			if (input[index] == '\0')
				continue;
			if (input[index] == '\n')
			{
				otLogInfoPlat("%s", input);
				otPlatUartReceived((uint8_t*)input, strlen(input));
				index = 0;
				memset(input, 0x00, DOWN_BUFFER_SIZE);
			}
			else
				index++;
		}
#endif

		taskYIELD();
	}
	otInstanceFinalize(sInstance);
	goto pseudo_reset;
}

char test_responseContent[30] = "hello";
// Handler for Test resource
void coap_handler_test(void* aContext, otMessage* aMessage, const otMessageInfo* aMessageInfo)
{
	otError error = OT_ERROR_NONE;
	otMessage* responseMessage;
	otCoapCode responseCode = OT_COAP_CODE_EMPTY;

	responseCode = OT_COAP_CODE_CONTENT;
	responseMessage = otCoapNewMessage(sInstance, NULL);
	VerifyOrExit(responseMessage != NULL, error = OT_ERROR_NO_BUFS);

	if (otCoapMessageGetCode(aMessage) == OT_COAP_CODE_POST)
	{
		uint16_t length = otMessageGetLength(aMessage) - otMessageGetOffset(aMessage);

		otMessageRead(aMessage, otMessageGetOffset(aMessage), test_responseContent, length);
		test_responseContent[length] = '\0';
	}

	otCoapMessageInit(responseMessage, OT_COAP_TYPE_ACKNOWLEDGMENT, responseCode);
	otCoapMessageSetMessageId(responseMessage, otCoapMessageGetMessageId(aMessage));
	otCoapMessageSetToken(responseMessage, otCoapMessageGetToken(aMessage), otCoapMessageGetTokenLength(aMessage));
	otCoapMessageSetPayloadMarker(responseMessage);
	SuccessOrExit(error = otMessageAppend(responseMessage, &test_responseContent, sizeof(test_responseContent)));
	SuccessOrExit(error = otCoapSendResponse(sInstance, responseMessage, aMessageInfo));

exit:

	if (error != OT_ERROR_NONE)
	{
		if (responseMessage != NULL)
		{
			otLogInfoPlat("coap send response error %d: %s\r\n", error, otThreadErrorToString(error));
			otMessageFree(responseMessage);
		}
	}
	else if (responseCode >= OT_COAP_CODE_RESPONSE_MIN)
	{
		otLogInfoPlat("coap response sent successfully!\r\n");
	}
}
void coap_handler_mode(void* aContext, otMessage* aMessage, const otMessageInfo* aMessageInfo)
{
	otError error = OT_ERROR_NONE;
	otMessage* responseMessage;
	otCoapCode responseCode = OT_COAP_CODE_EMPTY;
	char responseContent[30] = "0";

	responseCode = OT_COAP_CODE_CONTENT;
	responseMessage = otCoapNewMessage(sInstance, NULL);
	VerifyOrExit(responseMessage != NULL, error = OT_ERROR_NO_BUFS);

	if (otCoapMessageGetCode(aMessage) == OT_COAP_CODE_POST)
	{
		uint16_t length = otMessageGetLength(aMessage) - otMessageGetOffset(aMessage);
		uint8_t buf[10];

		otMessageRead(aMessage, otMessageGetOffset(aMessage), buf, length);
		buf[length] = '\0';

		if (length > 0 && length < sizeof(buf))
		{
			if (strcmp((const char*)buf, "off") == 0)
			{
				app_mode = appMode_Manual;
				otLogInfoPlat("set device mode to off");
			}
			else if (strcmp((const char*)buf, "single") == 0)
			{
				app_mode = appMode_Single;
				otLogInfoPlat("set device mode to single");
			}
			else if (strcmp((const char*)buf, "multi") == 0)
			{
				app_mode = appMode_Multiple;
				otLogInfoPlat("set device mode to multi");
				sntp_query();
			}
		}
	}

	switch (app_mode)
	{
		case appMode_Manual:
		{
			sprintf(responseContent, "mode: off");
			break;
		}
		case appMode_Single:
		{
			sprintf(responseContent, "mode: single");
			break;
		}
		case appMode_Multiple:
		{
			sprintf(responseContent, "mode: multi");
			break;
		}
		default: break;
	}

	otCoapMessageInit(responseMessage, OT_COAP_TYPE_ACKNOWLEDGMENT, responseCode);
	otCoapMessageSetMessageId(responseMessage, otCoapMessageGetMessageId(aMessage));
	otCoapMessageSetToken(responseMessage, otCoapMessageGetToken(aMessage), otCoapMessageGetTokenLength(aMessage));
	otCoapMessageSetPayloadMarker(responseMessage);
	SuccessOrExit(error = otMessageAppend(responseMessage, &responseContent, sizeof(responseContent)));
	SuccessOrExit(error = otCoapSendResponse(sInstance, responseMessage, aMessageInfo));

exit:

	if (error != OT_ERROR_NONE)
	{
		if (responseMessage != NULL)
		{
			otLogInfoPlat("coap send response error %d: %s\r\n", error, otThreadErrorToString(error));
			otMessageFree(responseMessage);
		}
	}
	else if (responseCode >= OT_COAP_CODE_RESPONSE_MIN)
	{
		otLogInfoPlat("coap response sent successfully!\r\n");
	}
}

// Handler for LED resource
void coap_handler_led(void* aContext, otMessage* aMessage, const otMessageInfo* aMessageInfo)
{
	otError error = OT_ERROR_NONE;
	otMessage* responseMessage;
	otCoapCode responseCode = OT_COAP_CODE_EMPTY;
	char responseContent[30] = "0";

	responseCode = OT_COAP_CODE_CONTENT;
	responseMessage = otCoapNewMessage(sInstance, NULL);
	VerifyOrExit(responseMessage != NULL, error = OT_ERROR_NO_BUFS);

	if (otCoapMessageGetCode(aMessage) == OT_COAP_CODE_POST)
	{
		uint16_t length = otMessageGetLength(aMessage) - otMessageGetOffset(aMessage);
		uint8_t buf[20];

		otMessageRead(aMessage, otMessageGetOffset(aMessage), buf, length);
		buf[length] = '\0';

		if (length > 0 && length < sizeof(buf))
		{
			if (strncmp((const char*)buf, "toggle", length) == 0)
			{
				otLogInfoPlat("toggle action");
				GPIO_TogglePinsOutput(BOARD_LED_GPIO, 1u << BOARD_LED_GPIO_PIN);
			};
			if (strncmp((const char*)buf, "on", length) == 0)
			{
				otLogInfoPlat("on action");
				GPIO_SetPinsOutput(BOARD_LED_GPIO, 1u << BOARD_LED_GPIO_PIN);
				led_on_timer = pdMS_TO_TICKS(10000);
			};
			if (strncmp((const char*)buf, "off", length) == 0)
			{
				otLogInfoPlat("off action");
				GPIO_ClearPinsOutput(BOARD_LED_GPIO, 1u << BOARD_LED_GPIO_PIN);
			};
			if (strncmp((const char*)buf, "from neighbor", length) == 0)
			{
				otLogInfoPlat("neighbor action");
				light_trigger = light_goes_down;
				GPIO_SetPinsOutput(BOARD_LED_GPIO, 1u << BOARD_LED_GPIO_PIN);
			};
		}
	}

	led_value = GPIO_ReadPinInput(BOARD_LED_GPIO, BOARD_LED_GPIO_PIN);
	sprintf(responseContent, "led now is %s", led_value ? " on" : "off");

	otCoapMessageInit(responseMessage, OT_COAP_TYPE_ACKNOWLEDGMENT, responseCode);
	otCoapMessageSetMessageId(responseMessage, otCoapMessageGetMessageId(aMessage));
	otCoapMessageSetToken(responseMessage, otCoapMessageGetToken(aMessage), otCoapMessageGetTokenLength(aMessage));
	otCoapMessageSetPayloadMarker(responseMessage);
	SuccessOrExit(error = otMessageAppend(responseMessage, &responseContent, sizeof(responseContent)));
	SuccessOrExit(error = otCoapSendResponse(sInstance, responseMessage, aMessageInfo));

exit:

	if (error != OT_ERROR_NONE)
	{
		if (responseMessage != NULL)
		{
			otLogInfoPlat("coap send response error %d: %s\r\n", error, otThreadErrorToString(error));
			otMessageFree(responseMessage);
		}
	}
	else if (responseCode >= OT_COAP_CODE_RESPONSE_MIN)
	{
		otLogInfoPlat("coap response sent successfully!\r\n");
	}
}

void coap_handler_level(void* aContext, otMessage* aMessage, const otMessageInfo* aMessageInfo)
{
	otError error = OT_ERROR_NONE;
	otMessage* responseMessage;
	otCoapCode responseCode = OT_COAP_CODE_EMPTY;
	char responseContent[30] = "0";

	responseCode = OT_COAP_CODE_CONTENT;
	responseMessage = otCoapNewMessage(sInstance, NULL);
	VerifyOrExit(responseMessage != NULL, error = OT_ERROR_NO_BUFS);

	if (otCoapMessageGetCode(aMessage) == OT_COAP_CODE_POST)
	{
		uint16_t length = otMessageGetLength(aMessage) - otMessageGetOffset(aMessage);
		uint8_t buf[6];

		if (length > 0 && length < sizeof(buf))
		{
			otMessageRead(aMessage, otMessageGetOffset(aMessage), buf, length);
			buf[length] = '\0';
			uint32_t trig_val = strtol((const char*)buf, NULL, 10);

			if (trig_val != 0)
			{
				light_lvl_trigger = trig_val;
				otLogInfoPlat("set trigger value to %lu", light_lvl_trigger);
			}
		}
	}

	otCoapMessageInit(responseMessage, OT_COAP_TYPE_ACKNOWLEDGMENT, responseCode);
	otCoapMessageSetMessageId(responseMessage, otCoapMessageGetMessageId(aMessage));
	otCoapMessageSetToken(responseMessage, otCoapMessageGetToken(aMessage), otCoapMessageGetTokenLength(aMessage));
	otCoapMessageSetPayloadMarker(responseMessage);
	SuccessOrExit(error = otMessageAppend(responseMessage, &responseContent, sizeof(responseContent)));
	SuccessOrExit(error = otCoapSendResponse(sInstance, responseMessage, aMessageInfo));

exit:

	if (error != OT_ERROR_NONE)
	{
		if (responseMessage != NULL)
		{
			otLogInfoPlat("coap send response error %d: %s\r\n", error, otThreadErrorToString(error));
			otMessageFree(responseMessage);
		}
	}
	else if (responseCode >= OT_COAP_CODE_RESPONSE_MIN)
	{
		otLogInfoPlat("coap response sent successfully!\r\n");
	}
}
// Handler for Index resource
void coap_handler_index(void* aContext, otMessage* aMessage, const otMessageInfo* aMessageInfo)
{
	otError error = OT_ERROR_NONE;
	otMessage* responseMessage;
	otCoapCode responseCode = OT_COAP_CODE_EMPTY;
	char responseContent[30] = "0";

	responseCode = OT_COAP_CODE_CONTENT;
	responseMessage = otCoapNewMessage(sInstance, NULL);
	VerifyOrExit(responseMessage != NULL, error = OT_ERROR_NO_BUFS);

	if (otCoapMessageGetCode(aMessage) == OT_COAP_CODE_POST)
	{
		otLogInfoPlat("post index");
		uint16_t length = otMessageGetLength(aMessage) - otMessageGetOffset(aMessage);
		uint8_t buf[6];

		if (length > 0 && length < sizeof(buf))
		{
			otMessageRead(aMessage, otMessageGetOffset(aMessage), buf, length);
			buf[length] = '\0';
			uint32_t light_index_val = strtol((const char*)buf, NULL, 10);

			if (light_index_val != 0)
			{
				light_index = light_index_val;
				otLogInfoPlat("set index to %lu", light_index);
			}
		}
	}
	sprintf(responseContent, "index: %lu", light_index);

	otCoapMessageInit(responseMessage, OT_COAP_TYPE_ACKNOWLEDGMENT, responseCode);
	otCoapMessageSetMessageId(responseMessage, otCoapMessageGetMessageId(aMessage));
	otCoapMessageSetToken(responseMessage, otCoapMessageGetToken(aMessage), otCoapMessageGetTokenLength(aMessage));
	otCoapMessageSetPayloadMarker(responseMessage);
	SuccessOrExit(error = otMessageAppend(responseMessage, &responseContent, sizeof(responseContent)));
	SuccessOrExit(error = otCoapSendResponse(sInstance, responseMessage, aMessageInfo));

exit:

	if (error != OT_ERROR_NONE)
	{
		if (responseMessage != NULL)
		{
			otLogInfoPlat("coap send response error %d: %s\r\n", error, otThreadErrorToString(error));
			otMessageFree(responseMessage);
		}
	}
	else if (responseCode >= OT_COAP_CODE_RESPONSE_MIN)
	{
		otLogInfoPlat("coap response sent successfully!\r\n");
	}
}

// Init all resources
void coapAppInit()
{
	// test resource
	error = otCoapStart(sInstance, OT_DEFAULT_COAP_PORT);
	cr_1.mUriPath = "test";
	cr_1.mHandler = &coap_handler_test;
	error = otCoapAddResource(sInstance, &cr_1);

	// led control
	cr_led.mUriPath = "led";
	cr_led.mHandler = &coap_handler_led;
	error = otCoapAddResource(sInstance, &cr_led);

	// mode control
	cr_mode.mUriPath = "mode";
	cr_mode.mHandler = &coap_handler_mode;
	error = otCoapAddResource(sInstance, &cr_mode);

	// light level control
	cr_lux_trig.mUriPath = "level";
	cr_lux_trig.mHandler = &coap_handler_level;
	error = otCoapAddResource(sInstance, &cr_lux_trig);

	// set index
	cr_index.mUriPath = "index";
	cr_index.mHandler = &coap_handler_index;
	error = otCoapAddResource(sInstance, &cr_index);

	otError error = otCoapSecureStart(sInstance, OT_DEFAULT_COAP_SECURE_PORT, NULL);
	error = otCoapSecureSetPsk(sInstance,
	                           (const uint8_t*)GCP_COAP_SECURE_PSK,
	                           strlen(GCP_COAP_SECURE_PSK),
	                           (const uint8_t*)GCP_COAP_SECURE_PSK_INDENTITY,
	                           strlen(GCP_COAP_SECURE_PSK_INDENTITY));

	otCoapSecureSetDefaultHandler(sInstance, coap_default_handler, NULL);
}

// Get readings from ADC
void getLightLevel()
{
	light_lvl_last[count] = light_level_adc;
	count++;
	ADC16_SetChannelConfig(DEMO_ADC16_BASE, DEMO_ADC16_CHANNEL_GROUP, &adc16ChannelConfigStruct);
	while (0U == (kADC16_ChannelConversionDoneFlag & ADC16_GetChannelStatusFlags(DEMO_ADC16_BASE, DEMO_ADC16_CHANNEL_GROUP)))
	{
	}
	light_level_adc = ADC16_GetChannelConversionValue(DEMO_ADC16_BASE, DEMO_ADC16_CHANNEL_GROUP);

	if (count == 10)
	{
		count = 0;
		uint32_t temp = 0;
		for (int i = 0; i < 10; i++)
			temp = temp + light_lvl_last[i];
		temp = temp / 10;
		if (temp < (light_lvl_trigger + light_lvl_gyst))
			light_trigger = light_goes_up;
		if (temp > (light_lvl_trigger - light_lvl_gyst))
			light_trigger = light_goes_down;
	}
}

#include "coap/coap_message.hpp"

void PrintPayload(otMessage* aMessage)
{
	uint8_t buf[16];
	uint16_t bytesToPrint;
	uint16_t bytesPrinted = 0;
	uint16_t length = otMessageGetLength(aMessage) - otMessageGetOffset(aMessage);

	if (length > 0)
	{
		while (length > 0)
		{
			bytesToPrint = (length < sizeof(buf)) ? length : sizeof(buf);
			otMessageRead(aMessage, otMessageGetOffset(aMessage) + bytesPrinted, buf, bytesToPrint);
			buf[bytesToPrint] = '\0';
			otLogInfoPlat("%s", buf);
			length -= bytesToPrint;
			bytesPrinted += bytesToPrint;
		}
	}
}

void parsePostMessage(otMessage* aMessage)
{
	uint8_t buf[32];
	uint16_t bytesToPrint;
	uint16_t bytesPrinted = 0;
	uint16_t length = otMessageGetLength(aMessage) - otMessageGetOffset(aMessage);

	if (length > 0 && length < sizeof(buf))
	{
		while (length > 0)
		{
			bytesToPrint = (length < sizeof(buf)) ? length : sizeof(buf);
			otMessageRead(aMessage, otMessageGetOffset(aMessage) + bytesPrinted, buf, bytesToPrint);
			length -= bytesToPrint;
			bytesPrinted += bytesToPrint;
		}
		buf[bytesToPrint] = '\0';
		otLogInfoPlat("%s", buf);
		//        led_process_query((const char *)buf, bytesToPrint);
	}
	else
	{
		otLogInfoPlat("No or too large payload");
	}
}

struct ipRecord
{
	uint32_t ipIndex;
	otIp6Address ipaddress;
};

ipRecord ipTable[20];
uint8_t ipTableIterator = 0;

void printTable()
{
	otLogInfoPlat("Index, [address]:");

	for (ipRecord record : ipTable)
	{
		otLogInfoPlat("%d, [%x:%x:%x:%x:%x:%x:%x:%x]",
		              record.ipIndex,
		              HostSwap16(record.ipaddress.mFields.m16[0]),
		              HostSwap16(record.ipaddress.mFields.m16[1]),
		              HostSwap16(record.ipaddress.mFields.m16[2]),
		              HostSwap16(record.ipaddress.mFields.m16[3]),
		              HostSwap16(record.ipaddress.mFields.m16[4]),
		              HostSwap16(record.ipaddress.mFields.m16[5]),
		              HostSwap16(record.ipaddress.mFields.m16[6]),
		              HostSwap16(record.ipaddress.mFields.m16[7]));
	}
}

void icmpcallback_handler(void* aContext, otMessage* aMessage, const otMessageInfo* aMessageInfo, const otIcmp6Header* aIcmpHeader)
{
	otLogInfoPlat("!!!! ICMPv6 RESPONSE!!!!!");
	otIp6Address null_addr;
	uint32_t addressInTable = 0;

	VerifyOrExit(aIcmpHeader->mType == OT_ICMP6_TYPE_ECHO_REPLY);

	//	mServer->OutputFormat("%d bytes from ", aMessage.GetLength() - aMessage.GetOffset() + sizeof(otIcmp6Header));
	//
	//	mServer->OutputFormat(
	//		"%x:%x:%x:%x:%x:%x:%x:%x", HostSwap16(aMessageInfo.GetPeerAddr().mFields.m16[0]),
	//		HostSwap16(aMessageInfo.GetPeerAddr().mFields.m16[1]), HostSwap16(aMessageInfo.GetPeerAddr().mFields.m16[2]),
	//		HostSwap16(aMessageInfo.GetPeerAddr().mFields.m16[3]), HostSwap16(aMessageInfo.GetPeerAddr().mFields.m16[4]),
	//		HostSwap16(aMessageInfo.GetPeerAddr().mFields.m16[5]), HostSwap16(aMessageInfo.GetPeerAddr().mFields.m16[6]),
	//		HostSwap16(aMessageInfo.GetPeerAddr().mFields.m16[7]));
	//	mServer->OutputFormat(": icmp_seq=%d hlim=%d", HostSwap16(aIcmpHeader.mData.m16[1]), aMessageInfo.mHopLimit);
	//
	//	if (aMessage.Read(aMessage.GetOffset(), sizeof(uint32_t), &timestamp) >= static_cast<int>(sizeof(uint32_t)))
	//	{
	//		mServer->OutputFormat(" time=%dms", TimerMilli::GetNow() - HostSwap32(timestamp));
	//	}
	//
	//	mServer->OutputFormat("\r\n");

	//	if (ipTableIterator > 10) ipTableIterator = 0;

	otIp6AddressFromString("0", &null_addr);

	for (ipRecord record : ipTable)
	{
		if (otIp6IsAddressEqual(&record.ipaddress, &aMessageInfo->mPeerAddr))
		{
			addressInTable = 1;
		}
	}

	if (addressInTable == 0)
	{

		for (uint8_t record = 0; record < 20; record++)
		{
			addressInTable = addressInTable;
			if (otIp6IsAddressEqual(&ipTable[record].ipaddress, &null_addr))
			{
				ipTable[record].ipaddress = aMessageInfo->mPeerAddr;
				otLogInfoPlat("save record");
				break;
			}
		}
	}
	//	ipTable[ipTableIterator].ipaddress = aMessageInfo->mPeerAddr;
	//	ipTableIterator++;

exit:
	return;
}

#include <common/timer.hpp>
#include <cli/cli.hpp>

void send_ping()
{
	otError error = OT_ERROR_NONE;
	uint32_t timestamp = HostSwap32(ot::TimerMilli::GetNow());
	uint16_t mLength = 8;
	const otMessageInfo* messageInfo;
	ot::Ip6::MessageInfo mMessageInfo;

	otMessage* message;
	SuccessOrExit(error = mMessageInfo.GetPeerAddr().FromString("FF03::1"));
	//    SuccessOrExit(error = mMessageInfo.GetPeerAddr().FromString("FF0A::FD"));

	mMessageInfo.mInterfaceId = OT_NETIF_INTERFACE_ID_THREAD;
	messageInfo = static_cast<const otMessageInfo*>(&mMessageInfo);

	VerifyOrExit((message = otIp6NewMessage(sInstance, NULL)) != NULL, error = OT_ERROR_NO_BUFS);
	SuccessOrExit(error = otMessageAppend(message, &timestamp, sizeof(timestamp)));
	SuccessOrExit(error = otMessageSetLength(message, mLength));
	SuccessOrExit(error = otIcmp6SendEchoRequest(sInstance, message, messageInfo, 1));

exit:

	if (error != OT_ERROR_NONE && message != NULL)
	{
		otMessageFree(message);
	}
}

void coapGetIndex_handler(void* aContext, otMessage* aMessage, const otMessageInfo* aMessageInfo, otError aError)
{
	otLogInfoPlat("~~~~~ COAP RESPONSE~~~~~~");
	if (aError != OT_ERROR_NONE)
	{
		otLogInfoPlat("coap receive response error %d: %s", aError, otThreadErrorToString(aError));
	}
	else
	{
		otLogInfoPlat("~~~~~ Valid response ~~~~~~");
		uint16_t length = otMessageGetLength(aMessage) - otMessageGetOffset(aMessage);
		uint8_t buf[16];
		otMessageRead(aMessage, otMessageGetOffset(aMessage), buf, length);
		buf[15] = '\0';
		if (strncmp((char*)buf, "index: ", 6) == 0)
		{
			uint32_t index_parsed = strtol(&((char*)buf)[6], NULL, 10);
			if (index_parsed != 0)
			{
				otLogInfoPlat("receive index %lu", index_parsed);
				for (uint8_t record = 0; record < 20; record++)
				{
					if (otIp6IsAddressEqual(&ipTable[record].ipaddress, &aMessageInfo->mPeerAddr) == 1)
					{
						ipTable[record].ipIndex = index_parsed;
					}
				}
			}
		}
	}
}

void get_indexes()
{
	otIp6Address null_addr;
	otIp6AddressFromString("0", &null_addr);
	for (uint8_t record = 0; record < 20; record++)
	{
		if (otIp6IsAddressEqual(&ipTable[record].ipaddress, &null_addr) == 0 && ipTable[record].ipIndex == 0)
		{
			otError error = OT_ERROR_NONE;
			otMessage* message = NULL;
			otMessageInfo messageInfo;
			uint16_t payloadLength = 0;

			// Default parameters
			char coapUri[32] = "index";
			otCoapType coapType = OT_COAP_TYPE_NON_CONFIRMABLE;
			otCoapCode coapCode = OT_COAP_CODE_GET;

			message = otCoapNewMessage(sInstance, NULL);
			VerifyOrExit(message != NULL, error = OT_ERROR_NO_BUFS);

			otCoapMessageInit(message, coapType, coapCode);
			otCoapMessageGenerateToken(message, 2);
			SuccessOrExit(error = otCoapMessageAppendUriPathOptions(message,
			                                                        coapUri)); // append option

			memset(&messageInfo, 0, sizeof(messageInfo));
			messageInfo.mPeerAddr = ipTable[record].ipaddress;
			messageInfo.mPeerPort = OT_DEFAULT_COAP_PORT;
			messageInfo.mInterfaceId = OT_NETIF_INTERFACE_ID_THREAD;

			if ((coapType == OT_COAP_TYPE_CONFIRMABLE) || (coapCode == OT_COAP_CODE_GET))
			{
				error = otCoapSendRequest(sInstance, message, &messageInfo, &coapGetIndex_handler, NULL);
			}
			else
			{
				error = otCoapSendRequest(sInstance, message, &messageInfo, NULL, NULL);
			}

		exit:

			if ((error != OT_ERROR_NONE) && (message != NULL))
			{
				otMessageFree(message);
			}
		}
	}
}

void index_lookup(int index)
{
	// filling
	send_ping();
	get_indexes();
	// printing
	printTable();
}

void control_neigbor(uint32_t neighbor_index, lightTrigger_t light_trigger)
{
	char responseContent[30] = "0";

	switch (light_trigger)
	{
		case light_goes_up:
		{
			sprintf(responseContent, "off");
			break;
		}
		case light_goes_down:
		{
			sprintf(responseContent, "from neighbor");
			break;
		}
		default: break;
	}

	for (uint8_t record = 0; record < 20; record++)
	{
		if (ipTable[record].ipIndex == neighbor_index)
		{
			otError error = OT_ERROR_NONE;
			otMessage* message = NULL;
			otMessageInfo messageInfo;

			// Default parameters
			char coapUri[32] = "led";
			otCoapType coapType = OT_COAP_TYPE_CONFIRMABLE;
			otCoapCode coapCode = OT_COAP_CODE_POST;

			message = otCoapNewMessage(sInstance, NULL);
			VerifyOrExit(message != NULL, error = OT_ERROR_NO_BUFS);

			otCoapMessageInit(message, coapType, coapCode);
			otCoapMessageGenerateToken(message, 2);
			SuccessOrExit(error = otCoapMessageAppendUriPathOptions(message,
			                                                        coapUri)); // append option
			otCoapMessageSetPayloadMarker(message);
			SuccessOrExit(error = otMessageAppend(message, &responseContent, strlen(responseContent)));

			memset(&messageInfo, 0, sizeof(messageInfo));
			messageInfo.mPeerAddr = ipTable[record].ipaddress;
			messageInfo.mPeerPort = OT_DEFAULT_COAP_PORT;
			messageInfo.mInterfaceId = OT_NETIF_INTERFACE_ID_THREAD;

			if ((coapType == OT_COAP_TYPE_CONFIRMABLE) || (coapCode == OT_COAP_CODE_POST))
			{
				error = otCoapSendRequest(sInstance, message, &messageInfo, NULL, NULL);
			}
			else
			{
				error = otCoapSendRequest(sInstance, message, &messageInfo, NULL, NULL);
			}

		exit:

			if ((error != OT_ERROR_NONE) && (message != NULL))
			{
				otMessageFree(message);
			}
		}
	}
}

// DTLS

static void sntp_response_handler(void* p_context, uint64_t unix_time, otError result)
{
	(void)p_context;

	if (result == OT_ERROR_NONE)
	{
		m_unix_time = unix_time;
	}
}

static void sntp_query(void)

{
	otError error;

	otMessageInfo message_info;
	memset(&message_info, 0, sizeof(message_info));

	message_info.mInterfaceId = OT_NETIF_INTERFACE_ID_THREAD;
	message_info.mPeerPort = SNTP_SERVER_PORT;
	error = otIp6AddressFromString(SNTP_SERVER_ADDRESS, &message_info.mPeerAddr);
	//    ASSERT(error == OT_ERROR_NONE);

	otSntpQuery query;
	query.mMessageInfo = &message_info;

	error = otSntpClientQuery(sInstance, &query, sntp_response_handler, NULL);
	//    ASSERT(error == OT_ERROR_NONE);
}

//
///***************************************************************************************************
// * @section JWT generation.
// **************************************************************************************************/

static otError base64_url_encode(uint8_t* p_output, uint16_t* p_output_len, const uint8_t* p_buff, uint16_t length)
{
	otError error = OT_ERROR_NONE;
	int result;
	size_t encoded_len = 0;

	result = mbedtls_base64_encode(p_output, *p_output_len, &encoded_len, p_buff, length);

	if (result != 0)
	{
		return OT_ERROR_NO_BUFS;
	}

	// JWT uses URI as defined in RFC4648, while mbedtls as is in RFC1421.
	for (uint32_t index = 0; index < encoded_len; index++)
	{
		if (p_output[index] == '+')
		{
			p_output[index] = '-';
		}
		else if (p_output[index] == '/')
		{
			p_output[index] = '_';
		}
		else if (p_output[index] == '=')
		{
			p_output[index] = 0;
			encoded_len = index;
			break;
		}
	}

	*p_output_len = encoded_len;

	return error;
}

static otError
jwt_create(uint8_t* p_output, uint16_t* p_output_len, const uint8_t* p_claims, uint16_t claims_len, const uint8_t* p_private_key, uint16_t private_key_len)
{
	otError error = OT_ERROR_NONE;
	uint8_t hash[32];
	uint8_t signature[JWT_SIGNATURE_SIZE];
	uint16_t signature_size = JWT_SIGNATURE_SIZE;
	uint16_t output_max_length = *p_output_len;
	uint16_t length;
	mbedtls_sha256_context context;

	// Encode JWT Header using Base64 URL.
	length = output_max_length;

	error = base64_url_encode(p_output, &length, (const uint8_t*)JWT_HEADER_TYPE_ES256, strlen(JWT_HEADER_TYPE_ES256));
	//    ASSERT(error == OT_ERROR_NONE);

	*p_output_len = length;

	// Append delimiter.
	p_output[*p_output_len] = JWT_DELIMETER;
	*p_output_len += 1;

	// Encode JWT Claim using Base64 URL.
	length = output_max_length - *p_output_len;

	error = base64_url_encode(p_output + *p_output_len, &length, p_claims, claims_len);
	//    ASSERT(error == OT_ERROR_NONE);

	*p_output_len += length;

	// Create SHA256 Hash from encoded JWT Header and JWT Claim.
	mbedtls_sha256_init(&context);
	mbedtls_sha256_starts_ret(&context, 0);
	mbedtls_sha256_update_ret(&context, p_output, *p_output_len);
	mbedtls_sha256_finish_ret(&context, hash);
	mbedtls_sha256_free(&context);

	// Append delimiter.
	p_output[*p_output_len] = JWT_DELIMETER;
	*p_output_len += 1;

	// Create ECDSA Sign.
	error = otCryptoEcdsaSign(signature, &signature_size, hash, sizeof(hash), p_private_key, private_key_len);
	//    ASSERT(error == OT_ERROR_NONE);

	// Encode JWT Sign using Base64 URL.
	length = output_max_length - *p_output_len;

	error = base64_url_encode(p_output + *p_output_len, &length, signature, signature_size);
	//    ASSERT(error == OT_ERROR_NONE);

	*p_output_len += length;

	return error;
}

void coap_secure_connect_handler(bool connected, void* aContext)
{
	if (connected)
	{
		if (m_config_requested)
		{
			otLogInfoPlat("Secure handler: now coap config get\n\n\n\n");
			m_config_requested = false;
			//			coap_config_get();
		}
		else
		{
			otLogInfoPlat("Secure handler: now coap setState\n\n\n\n");
			//			coap_publish();
			//            coap_set_state();
		}
	}
}

#define BUFFER_LENGTH 512

static void coap_header_proxy_uri_append(otMessage* aMessage, const char* p_action)
{
	char jwt[BUFFER_LENGTH];
	char claims[BUFFER_LENGTH];

	memset(jwt, 0, sizeof(jwt));
	memset(claims, 0, sizeof(claims));

	uint16_t offset = snprintf(jwt,
	                           sizeof(jwt),
	                           "%s/%s/%s/%s/%s?jwt=",
	                           GCP_COAP_IOT_CORE_PROJECT_ID,
	                           GCP_COAP_IOT_CORE_REGION,
	                           GCP_COAP_IOT_CORE_REGISTRY_ID,
	                           GCP_COAP_IOT_CORE_DEVICE_ID,
	                           p_action);

	uint16_t output_len = sizeof(jwt) - offset;

	uint64_t timeout = m_unix_time + (SNTP_QUERY_INTERVAL / 1000) * 2;

	snprintf(claims, sizeof(claims), "{\"iat\":%ld,\"exp\":%ld,\"aud\":\"%s\"}", (uint32_t)(m_unix_time), (uint32_t)(timeout), GCP_COAP_IOT_CORE_PROJECT_ID);

	otError error = jwt_create((uint8_t*)&jwt[offset],
	                           &output_len,
	                           (const uint8_t*)claims,
	                           strlen(claims),
	                           (const uint8_t*)GCP_COAP_IOT_CORE_DEVICE_KEY,
	                           sizeof(GCP_COAP_IOT_CORE_DEVICE_KEY));
	//    ASSERT(error == OT_ERROR_NONE);
	otLogInfoPlat("Append JWT: %s \r\n", jwt);
	otCoapMessageAppendProxyUriOption(aMessage, jwt);
}

static void coap_payload_append(otMessage* p_message)
{
	char payload[BUFFER_LENGTH];

	memset(payload, 0, sizeof(payload));

	uint16_t length =
	    snprintf(payload, sizeof(payload), "{\"led state\":%d,\"mode is\":%d,\"index is\":%d,\"delay is\":%d}", led_value, app_mode, light_index, delay_val);

	otError error = otMessageAppend(p_message, &payload, length);
	//    ASSERT(error == OT_ERROR_NONE);
}

void coap_secure_connect(void)
{
	otSockAddr sock_addr;
	memset(&sock_addr, 0, sizeof(sock_addr));

	sock_addr.mPort = GCP_COAP_IOT_CORE_SERVER_SECURE_PORT;
	otIp6AddressFromString(GCP_COAP_IOT_CORE_SERVER_ADDRESS, &sock_addr.mAddress);

	otError error = otCoapSecureConnect(sInstance, &sock_addr, coap_secure_connect_handler, NULL);

	otLogInfoPlat("error is %d", error);
	//    ASSERT(error == OT_ERROR_NONE);
}

/**@brief Default CoAP Handler. */
static void coap_default_handler(void* p_context, otMessage* p_message, const otMessageInfo* p_message_info)
{
	(void)p_context;
	(void)p_message;
	(void)p_message_info;

	otLogInfoPlat("Received CoAP message that does not match any request or resource\r\n");
}

//
///**@brief CoAP response handler. */
static void coap_response_handler(void* p_context, otMessage* p_message, const otMessageInfo* p_message_info, otError result)
{
	(void)p_message_info;

	otCoapType testType = otCoapMessageGetType(p_message);
	otCoapCode testCode = otCoapMessageGetCode(p_message);
	otLogInfoPlat("Type %d, Code %d\r\n", testType, testCode);
	// Handle payload only if its response for config option.
	bool sendState = false;
	if (1)
	{
		do
		{
			char config[BUFFER_LENGTH];
			uint16_t config_length = 0;
			uint32_t temp = 0;
			if ((config_length = otMessageRead(p_message, otMessageGetOffset(p_message), &config, BUFFER_LENGTH - 1)) == 0)
			{
				otLogInfoPlat("Config includes incorrect data\r\n");
				break;
			}

			config[config_length] = 0;
			if (strcmp((const char*)config, "ON") == 0)
			{
				GPIO_SetPinsOutput(BOARD_LED_GPIO, 1u << BOARD_LED_GPIO_PIN);
				temp = GPIO_ReadPinInput(BOARD_LED_GPIO, BOARD_LED_GPIO_PIN);
				if (temp != led_value)
				{
					led_value = temp;
					sendState = true;
				}
			}
			else if (strcmp((const char*)config, "OFF") == 0)
			{
				GPIO_ClearPinsOutput(BOARD_LED_GPIO, 1u << BOARD_LED_GPIO_PIN);
				temp = GPIO_ReadPinInput(BOARD_LED_GPIO, BOARD_LED_GPIO_PIN);
				if (temp != led_value)
				{
					led_value = temp;
					sendState = true;
				}
			}
			else if (strncmp((const char*)config, "delay", 5) == 0)
			{
				temp = strtol((const char*)config + 6, NULL, 10);
				if (temp != delay_val)
				{
					delay_val = temp;
					sendState = true;
				}
			}
			else if (strncmp((const char*)config, "mode", 4) == 0)
			{
				if (strcmp((const char*)config + 5, "off") == 0)
				{
					temp = appMode_Manual;
					otLogInfoPlat("set device mode to off");
				}
				else if (strcmp((const char*)config + 5, "single") == 0)
				{
					temp = appMode_Single;
					otLogInfoPlat("set device mode to single");
				}
				else if (strcmp((const char*)config + 5, "multi") == 0)
				{
					temp = appMode_Multiple;
					otLogInfoPlat("set device mode to multi");
				}
				if ((appMode_t)temp != app_mode)
				{
					app_mode = (appMode_t)temp;
					sendState = true;
				}
			}
			else if (strncmp((const char*)config, "index", 5) == 0)
			{
				temp = strtol((const char*)config + 6, NULL, 10);
				if (temp != light_index)
				{
					light_index = temp;
					sendState = true;
				}
			}
			else
				break;
			if (sendState)
			{
				sntp_query();
				coap_set_state();
				sendState = false;
			}
			otLogInfoPlat("Parce config - %s\r\n", config);
		} while (false);
	}
}

// setState
static void coap_set_state(void)
{
	otError error = OT_ERROR_NONE;
	otMessage* p_message;
	otMessageInfo message_info;

	if (!otCoapSecureIsConnected(sInstance) && !otCoapSecureIsConnectionActive(sInstance))
	{
		coap_secure_connect();
		return;
	}

	do
	{
		p_message = otCoapNewMessage(sInstance, NULL);
		otCoapMessageInit(p_message, OT_COAP_TYPE_CONFIRMABLE, OT_COAP_CODE_POST);
		otCoapMessageGenerateToken(p_message, 2);
		error = otCoapMessageAppendUriPathOptions(p_message, GCP_COAP_IOT_CORE_PATH);

		// Append CoAP URI Proxy option that includes JSON Web Token.
		coap_header_proxy_uri_append(p_message, GCP_COAP_IOT_CORE_SETSTATE);

		otCoapMessageSetPayloadMarker(p_message);
		if (p_message == NULL)
		{
			break;
		}

		// Append CoAP Payload.
		coap_payload_append(p_message);

		// Set message info structure to point on GCP server.
		memset(&message_info, 0, sizeof(message_info));
		message_info.mInterfaceId = OT_NETIF_INTERFACE_ID_THREAD;
		message_info.mPeerPort = GCP_COAP_IOT_CORE_SERVER_SECURE_PORT;
		otIp6AddressFromString(GCP_COAP_IOT_CORE_SERVER_ADDRESS, &message_info.mPeerAddr);

		error = otCoapSecureSendRequest(sInstance, p_message, coap_response_handler, NULL);
	} while (false);

	if (error != OT_ERROR_NONE && p_message != NULL)
	{
		otMessageFree(p_message);
	}
}

// config
static void coap_config_get(void)
{
	otError error = OT_ERROR_NONE;
	otMessage* p_message;
	otMessageInfo message_info;

	if (!otCoapSecureIsConnected(sInstance) && !otCoapSecureIsConnectionActive(sInstance))
	{
		m_config_requested = true;
		coap_secure_connect();
		return;
	}

	do
	{
		p_message = otCoapNewMessage(sInstance, NULL);
		otCoapMessageInit(p_message, OT_COAP_TYPE_CONFIRMABLE, OT_COAP_CODE_GET);
		otCoapMessageGenerateToken(p_message, 2);
		error = otCoapMessageAppendUriPathOptions(p_message, GCP_COAP_IOT_CORE_PATH);
		// Append CoAP URI Proxy option that includes JSON Web Token.
		coap_header_proxy_uri_append(p_message, GCP_COAP_IOT_CORE_CONFIG);

		if (p_message == NULL)
		{
			break;
		}

		// Set message info structure to point on GCP server.
		memset(&message_info, 0, sizeof(message_info));
		message_info.mInterfaceId = OT_NETIF_INTERFACE_ID_THREAD;
		message_info.mPeerPort = GCP_COAP_IOT_CORE_SERVER_SECURE_PORT;
		otIp6AddressFromString(GCP_COAP_IOT_CORE_SERVER_ADDRESS, &message_info.mPeerAddr);

		error = otCoapSecureSendRequest(sInstance, p_message, coap_response_handler, NULL);
	} while (false);

	if (error != OT_ERROR_NONE && p_message != NULL)
	{
		otMessageFree(p_message);
	}
}

// publish
static void coap_publish(void)
{
	otError error = OT_ERROR_NONE;
	otMessage* p_message;
	otMessageInfo message_info;

	if (!otCoapSecureIsConnected(sInstance) && !otCoapSecureIsConnectionActive(sInstance))
	{
		coap_secure_connect();
		return;
	}

	do
	{
		p_message = otCoapNewMessage(sInstance, NULL);
		otCoapMessageInit(p_message, OT_COAP_TYPE_CONFIRMABLE, OT_COAP_CODE_POST);
		otCoapMessageGenerateToken(p_message, 2);
		error = otCoapMessageAppendUriPathOptions(p_message, GCP_COAP_IOT_CORE_PATH);

		// Append CoAP URI Proxy option that includes JSON Web Token.
		coap_header_proxy_uri_append(p_message, GCP_COAP_IOT_CORE_PUBLISH);

		otCoapMessageSetPayloadMarker(p_message);
		if (p_message == NULL)
		{
			break;
		}

		// Append CoAP Payload.
		coap_payload_append(p_message);

		// Set message info structure to point on GCP server.
		memset(&message_info, 0, sizeof(message_info));
		message_info.mInterfaceId = OT_NETIF_INTERFACE_ID_THREAD;
		message_info.mPeerPort = GCP_COAP_IOT_CORE_SERVER_SECURE_PORT;
		otIp6AddressFromString(GCP_COAP_IOT_CORE_SERVER_ADDRESS, &message_info.mPeerAddr);

		error = otCoapSecureSendRequest(sInstance, p_message, coap_response_handler, NULL);
	} while (false);

	if (error != OT_ERROR_NONE && p_message != NULL)
	{
		otMessageFree(p_message);
	}
}
