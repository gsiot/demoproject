/**
 *  @file
 *  @ingroup Group1
 *  @brief This file is in group1
 */


/**
 *  @defgroup Group1 Group for files 1
 */
#ifndef MKW41Z512XXX4_OT_HPP_
#define MKW41Z512XXX4_OT_HPP_


#include "openthread/src/core/common/locator.hpp"

#include <openthread/cli.h>
#include <openthread/ip6.h>
#include <openthread/udp.h>

#include "cli/cli_dataset.hpp"
#include "cli/cli_udp.hpp"

#if OPENTHREAD_ENABLE_APPLICATION_COAP
#include <coap/coap_message.hpp>
#include "cli/cli_coap.hpp"
#endif

#if OPENTHREAD_ENABLE_APPLICATION_COAP_SECURE
#include <coap/coap_message.hpp>
#include "cli/cli_coap_secure.hpp"
#endif

#include "common/code_utils.hpp"
#include "common/instance.hpp"

#ifndef OTDLL
#include <openthread/dns.h>
#include <openthread/sntp.h>
#include "common/timer.hpp"
#include "net/icmp6.hpp"
#endif

#include <openthread/coap.h>

#include "coap/coap_message.hpp"
#include "common/debug.hpp"
#include "openthread/src/core/common/locator.hpp"
#include "common/message.hpp"
#include "common/timer.hpp"
#include "net/ip6.hpp"
#include "net/netif.hpp"
#include "net/udp6.hpp"



/**
 * Энум аппмод
 * @ingroup Group2
 */
typedef enum
{
	appMode_Init,   //!< appMode_Init Инициализация
	appMode_Manual, //!< appMode_Manual Ручное
	appMode_Single, //!< appMode_Single Одинарное
	appMode_Multiple//!< appMode_Multiple Многочисленное
} appMode_t;

/**
 *  States of light demand
 *  @ingroup Group1
 */
typedef enum
{
	light_goes_down,	/*!< Light goes down flag */
	light_no_change,	/*!< Light stays at one level */
	light_goes_up		/*!< Light goes up, we do not need led */
} lightTrigger_t;




/**
 * @brief Main task function
 * @details 123
 * 123
 * 12
 * 3
 *
 * This is main task function, it makes a lot 123
 */

static void main_task(void *pvParameters);

/**
 *  @brief This is client application.
 *
 *  It essentially do what we need
 *
 *  @code{c}
 *
 *  // Example of using nvs_get_i32:
 * int32_t max_buffer_size = 4096; // default value
 *
 * @endcode
 *
 */

void lightControl();

#endif /* MKW41Z512XXX4_OT_HPP_ */
