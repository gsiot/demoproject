/*
 * mqttsn_net.h
 *
 *  Created on: May 31, 2018
 *      Author: Dmitry
 */

#ifndef MQTTSN_NET_H_
#define MQTTSN_NET_H_
/**
 * Важный инклуд
 */
#include "openthread-core-config.h"

//#include <openthread/types.h>
#include <openthread/udp.h>

#include <sys/time.h>

/**
 * @brief обычный класс
 * @ingroup group2
 */
class IPStack
{
public:
	/**
	 * 123
	 */
    IPStack();

    /**
     *  123
     * @param aString
     * @return
     */
    int Socket_error(const char* aString);
/**
 * 123
 * @param hostname
 * @param port
 * @return
 */
    int connect(const char* hostname, int port);
/**
 *
 * @param buffer
 * @param len
 * @param timeout_ms
 * @return
 */
    int read(unsigned char* buffer, int len, int timeout_ms);
/**
 *
 * @param buffer
 * @param len
 * @param timeout
 * @return
 */
    int write(unsigned char* buffer, int len, int timeout);
/**
 *
 * @return integer
 */
    int disconnect();

private:
 /**
  * 123123
  * @param aContext
  * @param aMessage
  * @param aMessageInfo
  */
    static void HandleUdpReceive(void *aContext, otMessage *aMessage, const otMessageInfo *aMessageInfo);
    void HandleUdpReceive(otMessage *aMessage, const otMessageInfo *aMessageInfo);

    otUdpSocket mSocket;
    otMessageInfo mPeer;
    otInstance *mInstance;

};

/**
 * 123123
 */
class Countdown
{
public:
    Countdown();
/**
 * 123123
 * @param ms
 */
    Countdown(int ms);

    bool expired();

    void countdown_ms(int ms);

    void countdown(int seconds);

    int left_ms();

private:

    struct timeval end_time;

};

#endif /* MQTTSN_NET_H_ */
